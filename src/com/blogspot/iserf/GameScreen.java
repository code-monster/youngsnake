package com.blogspot.iserf;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;
import javax.swing.Timer;

public class GameScreen extends JPanel implements ActionListener {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int OFFSET = 20;
	public static final int SEPARATOR = 1;
	public static final int SIZE_CELL = 12;
	public static final int FIELD_SIZE = 30;

	public static final int WIDTH = 400, HEIGHT = 400;
	public static final String DOWN = "Down";
	public static final String TOP = "Up";
	public static final String LEFT = "Left";
	public static final String RIGHT = "Right";

	private String keyDirection;
	private String direction;

	private Key key;

	private int speed = 120;
	Timer clock = new Timer(speed, this);
	private AppleManager<Apple> apples;

	private Snake<BodyPart> snake;

	private int xCoor, yCoor;

	public GameScreen() {
		setFocusable(true);
		key = new Key();
		addKeyListener(key);
		setPreferredSize(new Dimension(WIDTH, HEIGHT));

		snake = new Snake<BodyPart>();
		apples = new AppleManager<Apple>(FIELD_SIZE, SIZE_CELL);

		start();
	}

	private void start() {
		// TODO Auto-generated method stub
		xCoor = 0;
		yCoor = 0;

		direction = keyDirection = DOWN;

		snake.add(new BodyPart(xCoor, yCoor, SIZE_CELL));
		snake.add(new BodyPart(xCoor + 1, yCoor, SIZE_CELL));
		snake.add(new BodyPart(xCoor + 2, yCoor, SIZE_CELL));
		snake.add(new BodyPart(xCoor + 3, yCoor, SIZE_CELL));

		clock.start();
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		// TODO Auto-generated method stub
		// System.out.println(Math.random());

		apples.addAppleIfNot(snake);
		
		
		direction = keyDirection;
		snake.snakeGo(direction);

		if (apples.isCoordHasFoodAndEat(snake.get(snake.size() - 1).getxCoor(), snake
				.get(snake.size() - 1).getyCoor())) {

			snake.addElementToTail(SIZE_CELL);

		}

		if (snake.checkCrashWithWall(0, FIELD_SIZE - 1, 0, FIELD_SIZE - 1)
				|| snake.checkSnakeSuicide()) {
			stop();
			return;
		}

		repaint();
	}

	private void stop() {
		// TODO Auto-generated method stub
		snake.setAlive(false);
		repaint();
		clock.stop();
	}

	private void restart() {
		// TODO Auto-generated method stub
		snake.clear();
		apples.clear();
		snake.setAlive(true);
		start();
	}

	public void paint(Graphics g) {
		// System.out.println(Math.random());
		g.clearRect(OFFSET, OFFSET, WIDTH, HEIGHT);

		g.setColor(new Color(10, 50, 0));
		g.fillRect(0, 0, WIDTH, HEIGHT);

		g.setColor(Color.BLACK);
		for (int i = 0; i < FIELD_SIZE + 1; i++) {
			g.drawLine(OFFSET + i * SIZE_CELL, OFFSET, OFFSET + i * SIZE_CELL,
					OFFSET + SIZE_CELL * FIELD_SIZE);
		}

		for (int i = 0; i < FIELD_SIZE + 1; i++) {
			g.drawLine(OFFSET, OFFSET + i * SIZE_CELL, OFFSET + SIZE_CELL
					* FIELD_SIZE, OFFSET + i * SIZE_CELL);
		}

		snake.draw(g);

		apples.draw(g);

	}

	private class Key implements KeyListener {

		public void keyPressed(KeyEvent e) {

			int code = e.getKeyCode();

			if (KeyEvent.getKeyText(code) == GameScreen.DOWN
					&& direction != GameScreen.TOP) {
				keyDirection = DOWN;
			} else if (KeyEvent.getKeyText(code) == GameScreen.TOP
					&& direction != GameScreen.DOWN) {
				keyDirection = TOP;
			} else if (KeyEvent.getKeyText(code) == GameScreen.RIGHT
					&& direction != GameScreen.LEFT) {
				keyDirection = RIGHT;
			} else if (KeyEvent.getKeyText(code) == GameScreen.LEFT
					&& direction != GameScreen.RIGHT) {
				keyDirection = LEFT;
			} else if (KeyEvent.getKeyText(code) == "Space") {
				restart();
			}

		}

		@Override
		public void keyReleased(KeyEvent arg0) {
			// TODO Auto-generated method stub

		}

		@Override
		public void keyTyped(KeyEvent arg0) {
			// TODO Auto-generated method stub

		}

	}

}
